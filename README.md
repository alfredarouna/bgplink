# BGP Link Reputation Evaluator #

BGP Link Reputation Evaluator is an open-source application for graphical and intuitive representation of ***link reputation*** of the inter-domain routing and its evolution over time.

### Dependencies ###

* Custom version of [BGPlay.js](https://github.com/MaxCam/BGPlay) for [client side](client). 
* Server side with:
    * [BGPStream](https://github.com/caida/bgpstream) to get BGP records from multiple collectors.
    * [Bottle](https://github.com/bottlepy/bottle) as quick HTTP server.
    * [Team Cymru](https://www.team-cymru.org/) bogon reference, usefull to identify martians prefixes one each link.
*

### Quick start ###

Access [tests](tests) folder and follow steps to have running docker image ready for testing.

### Notes ###
*** !!! This project is in alpha phase and is not ready for production. Use it at your own risk.***

### TODOs ###

* User input control both on client and server side
* Ability to select time range
* Ability to select collectors based on project
* Timeline smooth evolution
