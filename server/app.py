"""
BGP Link Evaluator
Copyright (c) 2017 Alfred Arouna
"""

from bottle import Bottle, run, request, static_file, response

from bgpstream import bgpcollectorstream
from linktrust import linktrust

from bogons import bogoncymru
import json


import datetime
import time


'''
import datetime
import time


current = datetime.datetime.now() - datetime.timedelta(days=2)
yesterday = current - datetime.timedelta(days=5)
start_time = time.time()
timestr = time.strftime("%Y%m%d-%H%M%S")


def local_to_UTC(current, yesterday):
    """ Convert datetime to timestamp"""

    current_timestamp = time.mktime(current.timetuple())
    yesterday_timestamp = time.mktime(yesterday.timetuple())

    return {'query_starttime': int(yesterday_timestamp),
            'query_endtime': int(current_timestamp)}


#query_interval_filter = local_to_UTC(current, yesterday)
'''

app = Bottle()


# Benin Telecoms real time case
bt_realtime_start =  0
bt_realtime_end = time.mktime(datetime.datetime.now().timetuple())


# For testing
# http://www.onlineconversion.com/unix_time.htm


# 20:30 UTC 27 January 2011
#egypt_disconnect_start = 1296160200 
# 22:30 UTC 27 January 2011
#egypt_disconnect_end = 1296167400
# 8:30 UTC 2 February 2011
#egypt_connect_start = 1296635400
# 10:30 UTC 2 February 2011
#egypt_connect_end = 1296642600

# 21:00 UTC 27 January 2011
egypt_disconnect_start = 1296162000
# 23:00 UTC 27 January 2011
egypt_disconnect_end = 1296169200
# 8:30 UTC 2 February 2011
egypt_connect_start = 1296635400
# 10:30 UTC 2 February 2011
egypt_connect_end = 1296642600



# 19:00 UTC 24 February 2008
pakistan_hijack_sart = 1203879600
# 20:51 UTC 24 February 2008
pakistan_hijack_end = 1203886260
# 21:05 UTC 24 February 2008
pakistan_regular_sart = 1203887100
# 22:56 UTC 24 February 2008
pakistan_regular_end = 1203893760


# 08:00 UTC 24 August 2011
link_hijack_sart = 1314172800
# 10:00 UTC 24 August 2011
link_hijack_end = 1314180000
# 08:00 UTC 9 September 2011
link_regular_sart = 1315555200
# 10:00 UTC 9 September 2011
link_regular_end = 1315562400


jan_02_2016 = 1451692800
# 08:43 UTC, June 12th 2015
malaysia_leak_start = 1434098580
# 10:45 UTC, June 12th 2015
malaysia_leak_end = 1434105900
# 12:45 UTC, June 12th 2015
malaysia_regular_start = 1434113100
# 14:45 UTC, June 12th 2015
malaysia_regular_end = 1434120300


# Data query interval
pakistan_hijack_query_interval = {'query_starttime': pakistan_hijack_end,
                                  'query_endtime': pakistan_hijack_sart}

pakistan_regular_query_interval = {'query_starttime': pakistan_regular_end,
                                   'query_endtime': pakistan_regular_sart}

link_hijack_query_interval = {'query_starttime': link_hijack_end,
                              'query_endtime': link_hijack_sart}

link_regular_query_interval = {'query_starttime': link_regular_end,
                               'query_endtime': link_regular_end}


malaysia_leak_query_interval = {'query_starttime': malaysia_leak_end,
                                'query_endtime': malaysia_leak_start}

malaysia_regular_query_interval = {'query_starttime': malaysia_regular_end,
                                   'query_endtime': malaysia_regular_start}

egypt_disconnect_query_interval = {'query_starttime': egypt_disconnect_end,
                                'query_endtime': egypt_disconnect_start}

egypt_connect_query_interval = {'query_starttime': egypt_connect_end,
                                   'query_endtime': egypt_connect_start}

bt_query_interval = {'query_starttime': bt_realtime_start,
                                   'query_endtime': bt_realtime_end }                                   


#query_interval_filter = leak_query_interval_filter
#query_interval_filter = pakistan_regular_query_interval
#query_interval_filter = link_regular_query_interval
#query_interval_filter = malaysia_regular_query_interval
#query_interval_filter = egypt_disconnect_query_interval
query_interval_filter = bt_query_interval



@app.route('/')
def welcome():

    return {'data': 'json_records'}


@app.route('/datadirect.json')
def datadirect():
    """
    Route use for test data. We read local json file and 
    applied link evaluator algorithm.

    """

    callback = request.query.callback
    timestamp = request.query.unix_timestamps
    requesttype = request.query.type
    datatype = request.query.datatype
    response.content_type = 'application/json'

    content_json = json.load(open('data_' + datatype + '.json'))

    bgplink = linktrust.BGPLinkTrust(content_json['data'])

    # Get link reputation
    bgplink.get_link_reputation(datatype)


    return callback + '(' + json.dumps(content_json) + ')'


@app.route('/data.json')
def data():
    """
    Use to download BGP records using BGPStream.
    This data could be save in local json file.

    As we get the file, we apply link evaluator algorithm 
    and update specific json key in this file.

    """
    resource = request.query.resource
    response.content_type = 'application/json'
    callback = request.query.callback

    bgpstream = bgpcollectorstream.BGPCollectorStream(resource=resource,
                                                      interval_filter=query_interval_filter)

    data = bgpstream.collect_data()

    ''' Save in file '''
    '''
    #fileObj = open('data_'+time.strftime("%Y%m%d-%H%M%S")+'.json',"w")
    #fileObj = open('data_leak-pakistan.json', "w")
    fileObj = open('data_egypt_disconnect.json', "w")
    #fileObj = open('data_regular-pakistan_' + time.strftime("%Y%m%d") + '.json', "w")
    encoded = json.dumps(data)

    fileObj.write(encoded)
    fileObj.close()
    '''

    # return data to BGPlay.JS
    return callback + '(' + json.dumps(data) + ')'


run(app, host='0.0.0.0', port=8080, debug=True)


'''
"events": [
  {
  "timestamp": 1491060156,
  "type": "A",
  "attrs": {
    "source_id": "12-2001:7f8::b29:0:1",
    "path": [
        2857,
        286,
        1103,
        3333
    ]
   },
   "records": [
     {
      link:
      trust:
      attrs: {
       prefix_list: []
       as_path_list: []
      }
     },
     {
      link:
      trust:
      attrs: {
       prefix_list: []
       as_path_list: []
      }
     }
    ],
    
  }
},
'''
