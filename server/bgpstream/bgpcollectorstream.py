"""
BGP Link Evaluator
Copyright (c) 2017 Alfred Arouna
"""

from _pybgpstream import BGPStream, BGPRecord, BGPElem

import datetime
import time
import os


from itertools import groupby


current_date_time = datetime.datetime.now()
yesterday_date_time = current_date_time - datetime.timedelta(days=1)


class BGPCollectorStream(object):
    """
    Class to get BGP record using BGPStream python binding.
    """

    project = {'ris', 'routeviews'}
    collector_name = None
    record_type = {'ribs', 'updates'}
    collector_asn = None
    interval_filter = None
    resource = None

    def __init__(self, resource=resource, project=project,
                 collector_name=collector_name, record_type=record_type,
                 collector_asn=collector_asn, interval_filter=interval_filter):

        # New BGPStream object initiator

        self.resource = resource
        self.project = project
        self.collector_name = collector_name
        self.record_type = record_type
        self.collector_asn = collector_asn

        # result limitation counters
        self.records_number = 0
        self.elems_number = 0
        self.elm_count = 0

        # results
        self.sources = []
        self.nodes = []
        self.initial_state = []
        self.events = []
        self.targets = []

        # internal data collectors

        tmp_resource = []

        # Create a new bgpstream instance and a reusable bgprecord instance
        self.stream = BGPStream()
        self.rec = BGPRecord()


        if self.resource is not None:
            # Validation of prefix using iptools...later
            ''' USE CORRECT Annonced prefix MATCH'''
            for r in self.resource.split(','):
                tmp_resource.append(r)
                self.stream.add_filter('prefix', r)
                print r

        

        if interval_filter is not None:
            self.interval_filter = interval_filter
        else:

            self.interval_filter = self.local_to_UTC(current_date_time,
                                                     yesterday_date_time)

        self.stream.add_interval_filter(self.interval_filter['query_endtime'],
                                        self.interval_filter['query_starttime'])

        if self.interval_filter['query_starttime'] == 0:
            self.stream.set_live_mode()

        if self.project is not None:
            if isinstance(self.project, basestring):
                self.stream.add_filter('project', self.project)
            else:
                for project in self.project:
                    self.stream.add_filter('project', project)

        if collector_name is not None:
            self.stream.add_filter('collector', self.collector_name)

        if self.record_type is not None:
            if isinstance(self.record_type, basestring):
                self.stream.add_filter('record-type', self.record_type)
            else:
                for record in self.record_type:
                    self.stream.add_filter('record-type', record)

        if self.collector_asn is not None:
            self.stream.add_filter('peer-asn', self.collector_asn)

        # Set BGP time interval between two consecutive RIB
        # self.stream.add_rib_period_filter(30)

        # Default collectors
        ris_rrc = [00, 01, 02, 05, 06, 07, 10, 11]
        routeviews = ['bgplay', 'route-views', 'route-views4', 'route-views.kixp',
                      'route-views.linx', 'route-views.wide', 'route-views.sydney', 'route-views.saopaulo']
        for rrc in ris_rrc:
            self.stream.add_filter('collector', 'rrc' + str(rrc))
        for rv in routeviews:
            self.stream.add_filter('collector', rv)

        # Not yet supported
        # self.stream.add_filter('ipversion', '4')

        # Get ALL prefixes related to a given AS: still need RIPESTAT data
        # https://stat.ripe.net/data/prefix-overview/data.json?resource=41.222.192.28
        # https://stat.ripe.net/data/announced-prefixes/data.json?resource=AS37090&starttime=2011-12-12T12:00

        self.response = {'query_starttime': self.interval_filter['query_endtime'],
                         'query_endtime': self.interval_filter['query_starttime'],
                         'sources': self.sources, 'nodes': self.nodes,
                         'resource': tmp_resource, 'initial_state': self.initial_state, 'events': self.events,
                         'targets': self.targets, 'trust_min': 0, 'trust_max': 0}

    def collect_data(self):
        """
        Collect BGP records and set in in a json format similar to the one use by BGPlayJS. 
        Add new json key:value pair:
            - trust_min: minimal value for link evaluation cost.
            - trust_max: maximal value for link evaluation cost
            - records: new json data that contain link details for each BGP event
                - trust: link evaluation cost.
                - link: pair of connected as.
                - attrs:
                    - prefix_list: list of all prefix using the link.
                    - as_path_list: list of all AS PATH using the link.

        """

        self.stream.start()

        while (self.stream.get_next_record(self.rec)):


            ''' record_type could be rib or update'''
            record_type = self.rec.type

            ''' Get only correct records'''
            if self.rec.status == 'valid':
                elem = self.rec.get_next_elem()

                ''' Placeholder values '''
                hops = None

                '''elem could be None, don't waste my cpu'''
                if elem is None:
                    continue
                else:

                    self.records_number += 1

                    while (elem):

                        self.elems_number += 1

                        initial_state = True
                        in_source = False
                        self.elm_count += 1
                        node_exist = False
                        link_pfx = dict()
                        existing_timestamp_id = False
                        same_type_id = False
                        link_id = False
                        new_as_path = True
                        in_targets = False
                        old_record = False

                        '''
                        https://github.com/CAIDA/bgpstream/blob/master/lib/bgpstream_elem.c
                        switch (type) {
                          case BGPSTREAM_ELEM_TYPE_RIB:
                            buf[0] = 'R';
                            break;

                          case BGPSTREAM_ELEM_TYPE_ANNOUNCEMENT:
                            buf[0] = 'A';
                            break;

                          case BGPSTREAM_ELEM_TYPE_WITHDRAWAL:
                            buf[0] = 'W';
                            break;

                          case BGPSTREAM_ELEM_TYPE_PEERSTATE:
                            buf[0] = 'S';
                            break;

                          default:
                            buf[0] = '\0';
                            break;
                          }
                        '''

                        if elem.type in ['R', 'A', 'W']:

                            if elem.type == 'R' or elem.type == 'A':

                                full_hops = [k for k, g in groupby(
                                    elem.fields['as-path'].split(" "))]

                                # Remove as_set
                                hops = [h for h in full_hops if '{' not in h]

                            elif elem.type == 'W':
                                # Bypass following control :)
                                hops = [elem.peer_asn, elem.peer_asn]

                            # Create source dict component'''
                            source_id = self.rec.collector + '-' + elem.peer_address

                            if elem.type == 'W':
                                as_path = None
                            else:
                                as_path = map(int, hops)

                            ''' Sources records '''
                            for s in self.sources:
                                if source_id in s.values():
                                    in_source = True
                                    break

                            if not in_source:
                                self.sources.append({'as_number': elem.peer_asn,
                                                     'ip': elem.peer_address, 'rrc': self.rec.collector,
                                                     'id': source_id, 'project': self.rec.project})

                            ''' Targets '''
                            for t in self.targets:
                                if elem.fields['prefix'] in t.values():
                                    in_targets = True
                                    break

                            if not in_targets:
                                self.targets.append(
                                    {'prefix': elem.fields['prefix']})

                            if len(hops) > 1 and int(hops[0]) == elem.peer_asn:

                                ''' Initial state records '''
                                for state in self.initial_state:
                                    if source_id in state.values():
                                        initial_state = False
                                        break

                                ''' Initial state: first message of each collector'''
                                if initial_state:
                                    if as_path is not None:
                                        self.initial_state.append({'source_id': source_id, 'path': as_path, 'trust': None,
                                                                   'timestamp': elem.time, 'target_prefix': elem.fields['prefix']})

                                # Go through nodes
                                length = len(hops)
                                for i in range(0, length):
                                    # Get all AS: graph nodes
                                    if (i + 1) < length:
                                        # Skip custom withdrawn hops
                                        if hops[i] != hops[i + 1]:
                                            link = str(hops[i]) + ':' + \
                                                str(hops[i + 1])
                                        else:
                                            link = None

                                    if len(self.nodes) > 0:
                                        for n in self.nodes:
                                            if int(hops[i]) == n['as_number']:
                                                node_exist = True
                                                break
                                            else:
                                                node_exist = False

                                    if not node_exist:
                                        self.nodes.append(
                                            {'owner': 'Unknown', 'as_number': int(hops[i])})
                                    node_exist = False

                                    for data in self.events:
                                        if elem.time == data['timestamp']:
                                            if elem.type == data['type']:
                                                if as_path == data['attrs']['path']:
                                                    existing_timestamp_id = self.events.index(
                                                        data)
                                                    break

                                    # We have record
                                    if existing_timestamp_id or existing_timestamp_id is 0:

                                        if elem.type != 'W':

                                            # Check link
                                            for record in self.events[existing_timestamp_id][
                                                    'records']:
                                                # Same link
                                                if record['link'] == link:

                                                    old_record = True

                                                    if elem.fields['prefix'] not in record['attrs']['prefix_list']:
                                                        record[
                                                            'attrs']['prefix_list'].append(elem.fields['prefix'])
                                                    if as_path not in record['attrs']['as_path_list']:
                                                        record[
                                                            'attrs']['as_path_list'].append(as_path)
                                                    break

                                            if not old_record:

                                                # new record
                                                record = {}
                                                record[
                                                    'link'] = link
                                                record['trust'] = 0
                                                record[
                                                    'attrs'] = {}
                                                record['attrs'][
                                                    'prefix_list'] = [elem.fields['prefix']]
                                                record['attrs'][
                                                    'as_path_list'] = [as_path]

                                                self.events[existing_timestamp_id][
                                                    'records'].append(record)

                                        else:
                                            # Withdrawn
                                            withdrawn_link = self.get_widthdrawn_infos(
                                                self.events, elem.fields['prefix'])
                                            withdrawn_exist = set()
                                            # If we have existing record,
                                            # append the new prefix

                                            # if we still have link, create
                                            # new records
                                            if len(withdrawn_link) > 0:
                                                for w in withdrawn_link:
                                                    for record in self.events[existing_timestamp_id][
                                                            'records']:
                                                        # Same link
                                                        if record['link'] == w:
                                                            withdrawn_exist.add(
                                                                w)

                                                            if elem.fields['prefix'] not in record['attrs']['prefix_list']:
                                                                record[
                                                                    'attrs']['prefix_list'].append(elem.fields['prefix'])

                                                withdrawn_diff = withdrawn_link.difference(
                                                    withdrawn_exist)

                                                if len(withdrawn_diff) > 0:
                                                    for w in withdrawn_diff:
                                                        record = {}
                                                        record[
                                                            'link'] = w
                                                        record['trust'] = 0
                                                        record[
                                                            'attrs'] = {}
                                                        record['attrs'][
                                                            'prefix_list'] = [elem.fields['prefix']]
                                                        record['attrs'][
                                                            'as_path_list'] = []
                                                        self.events[existing_timestamp_id][
                                                            'records'].append(record)

                                    else:
                                        # New event
                                        data = self.generate_event_data(
                                            elem, source_id, link, as_path)

                                        self.events.append(
                                            {'timestamp': elem.time, 'type': elem.type, 'attrs': data['attrs'], 'records': data['records']})

                        elem = self.rec.get_next_elem()

        print 'BGP events: ' + str(self.elm_count)
        print 'Records count: ' + str(self.records_number)
        print 'Elems count: ' + str(self.elems_number)

        self.response['elems_number'] = self.elems_number
        self.response['records_number'] = self.records_number

        return {'data': self.response}

    def get_widthdrawn_infos(self, dict_array, prefix):
        """
        For each withdrawn prefix, check if prefix is known on
        previous data as target_prefix. If then, get all links
        to the set
        """

        link_list = set()

        for dict_elm in dict_array:
            if dict_elm['type'] == 'A':
                if prefix == dict_elm['attrs']['target_prefix']:
                    for record in dict_elm['records']:
                        link_list.add(record['link'])
        return link_list

    def generate_event_data(self, elem, source, link, as_path):
        """
        Create new record json data.
            - trust: link evaluation cost.
            - link: pair of connected as.
            - attrs:
                - prefix_list: list of all prefix using the link.
                - as_path_list: list of all AS PATH using the link.
        """
        attrs = {}
        attrs['source_id'] = source
        attrs['target_prefix'] = elem.fields['prefix']
        records = []

        if elem.type == 'W':
            withdrawn_link = self.get_widthdrawn_infos(
                self.events, elem.fields['prefix'])

            attrs['path'] = None

            if type(withdrawn_link) is set:
                for w in withdrawn_link:
                    record = {}
                    record['link'] = w
                    record['trust'] = 0
                    record['attrs'] = {}
                    record['attrs'][
                        'prefix_list'] = [elem.fields['prefix']]
                    record['attrs'][
                        'as_path_list'] = []

                    records.append(record)
            else:
                # NA means there is withdrawn on prefix
                # without A ou R during time interval.
                # We could not handle that.
                '''
                record = {}
                record['link'] = 'NA'
                record['trust'] = 0
                record['attrs'] = {}
                record['attrs'][
                    'prefix_list'] = [elem.fields['prefix']]
                record['attrs'][
                    'as_path_list'] = []

                records.append(record)
                '''

        else:
            attrs['path'] = as_path
            record = {}
            record['link'] = link
            record['trust'] = 0
            record['attrs'] = {}
            record['attrs'][
                'prefix_list'] = [elem.fields['prefix']]
            record['attrs'][
                'as_path_list'] = [as_path]

            records.append(record)

        return {'attrs': attrs, 'records': records}

    def local_to_UTC(self, current, yesterday):
        """ Get timestamp of current datetime as query_endtime. Get last day
        timestamp as query_starttime. This threshold of one date is similar to
        the one use by BGPlayJS on RIPESTAT"""

        current_timestamp = time.mktime(current.timetuple())
        yesterday_timestamp = time.mktime(yesterday.timetuple())

        return {'query_starttime': int(yesterday_timestamp),
                'query_endtime': int(current_timestamp)}
