BGPLink (server)
===============

This is the server part of BGP Link Reputation Evaluator project. It use [BGPStream](https://github.com/caida/bgpstream) from CAIDA as backend tool to grab BGP records from differents collectors. With those data, we can compute some predefined metrics on each ***link*** (between two AS). This projet also required [Team Cymru](https://www.team-cymru.org/) bogon reference, usefull to identify martians prefixes one each link.

Those records are formated as required by [BGPLink.js](../client) in JSON format. This format is similar to the one use by [BGPlay.js](https://github.com/MaxCam/BGPlay) on RIPEStats.

It has been developed as a part of the Master’s thesis of Alfred Arouna at Centre de Formation et de Recherche en Informatique (CeFRI) of University d'Abomey-Calavi (Benin).

The project is ***open source, see LICENSE.txt*** for more information.

