"""
BGP Link Evaluator
Copyright (c) 2017 Alfred Arouna
"""

from bogons import bogoncymru
from analytics import datascience


class BGPLinkTrust(object):
    """
    Class to apply link evaluator algorithm on collected data.

    """

    def __init__(self, data):

        self.data = data
        self.events = self.data['events']
        self.link_analytics = []

    def get_path_count(self):
        """
        Get link rank. This function is just an example to show
        how rank can be get. It's not use actually.

        """

        path_count = {}
        for events in self.events:
            if events['timestamp'] not in path_count.keys():
                path_count[events['timestamp']] = {}
            for record in events['records']:
                if record['link'] not in path_count[events['timestamp']].keys():
                    path_count[events['timestamp']][record['link']] = {}
                    if 'c' not in path_count[events['timestamp']][record['link']].keys():
                        path_count[events['timestamp']][record['link']][
                            'c'] = len(record['attrs']['as_path_list'])
                    else:
                        path_count[events['timestamp']][record['link']][
                            'c'] += len(record['attrs']['as_path_list'])

        return path_count

    def get_type_count(self):
        """
        Get link stability. This function is just an example to show
        how stability can be get. It's not use actually.

        """

        type_count = {}
        for events in self.events:
            if events['timestamp'] not in type_count.keys():
                type_count[events['timestamp']] = {}
            for record in events['records']:
                if record['link'] not in type_count[events['timestamp']].keys():
                    type_count[events['timestamp']][record['link']] = {}

                if events['type'] != 'W':
                    if 'n' not in type_count[events['timestamp']][record['link']].keys():
                        type_count[events['timestamp']][
                            record['link']]['n'] = 1
                    else:

                        type_count[events['timestamp']][
                            record['link']]['n'] += 1
                else:
                    if 'w' not in type_count[events['timestamp']][record['link']].keys():
                        type_count[events['timestamp']][
                            record['link']]['w'] = 1
                    else:

                        type_count[events['timestamp']][
                            record['link']]['w'] += 1

        return type_count

    def get_bogon_count(self):
        """
        Get link bogon count. This function is just an example to show
        how bogon count can be get. It's not use actually.

        """
        bogon_count = {}
        bogon = bogoncymru.BogonSet()
        bogon_set = bogon.read_bogon(
            'https://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt', True)
        for events in self.events:
            if events['timestamp'] not in bogon_count.keys():
                bogon_count[events['timestamp']] = {}
            for record in events['records']:
                if record['link'] not in path_count[events['timestamp']].keys():
                    path_count[events['timestamp']]['link'] = {}
                    if record['target_prefix'] in bogon_set:
                        if 'b' not in path_count[events['timestamp']]['link'].keys():
                            path_count[events['timestamp']]['link']['b'] = 1
                        else:
                            path_count[events['timestamp']]['link']['b'] += 1

        return bogon_count

    def get_link_reputation(self, datatype):
        """
        Get link reputation cost. For each link, this function get
        all three metrics and apply link evaluator algorithm to get
        this link reputation cost. Each link 'trust' key is updated
        with the cost on the collected data json.

        This function also update min and max value of link reputation cost
        on collected data json.

        """
        metrics = {}

        bogon = bogoncymru.BogonSet()
        bogon_set = bogon.read_bogon(True)

        cost_min = 0
        cost_max = 0

        previous_cnt = {}

        # Only for dataset creation
        #analytics = datascience.DataAnalytics(datatype)

        for events in self.events:
            if events['timestamp'] not in metrics.keys():
                metrics[events['timestamp']] = {}

            for record in events['records']:

                if record['link'] not in metrics[events['timestamp']].keys():
                    metrics[events['timestamp']][record['link']] = {}

                # Get link count
                if 'c' not in metrics[events['timestamp']][record['link']].keys():
                    metrics[events['timestamp']][record['link']][
                        'c'] = len(record['attrs']['as_path_list'])
                else:
                    metrics[events['timestamp']][record['link']][
                        'c'] += len(record['attrs']['as_path_list'])

                if record['link'] not in previous_cnt.keys():
                    previous_cnt[record['link']] = metrics[
                        events['timestamp']][record['link']]['c']
                else:
                    previous_cnt[record['link']] = metrics[events['timestamp']][
                        record['link']]['c'] - previous_cnt[record['link']]

                metrics[events['timestamp']][record['link']][
                    'r'] = previous_cnt[record['link']]
                previous_cnt[record['link']] = metrics[
                    events['timestamp']][record['link']]['c']

                # Get type count
                if 'n' not in metrics[events['timestamp']][record['link']].keys():
                    metrics[events['timestamp']][
                        record['link']]['n'] = 0
                if 'w' not in metrics[events['timestamp']][record['link']].keys():
                    metrics[events['timestamp']][
                        record['link']]['w'] = 0
                if events['type'] != 'W':
                    metrics[events['timestamp']][
                        record['link']]['n'] += 1
                else:
                    metrics[events['timestamp']][
                        record['link']]['w'] += -1

                metrics[events['timestamp']][record['link']]['s'] = metrics[events['timestamp']][
                    record['link']]['n'] + metrics[events['timestamp']][record['link']]['w']

                # Get bogon
                if 'b' not in metrics[events['timestamp']][record['link']].keys():
                    metrics[events['timestamp']][
                        record['link']]['b'] = 0
                for prefix in record['attrs']['prefix_list']:
                    if bogon_set is not None:
                        if prefix in bogon_set:
                            metrics[events['timestamp']][
                                record['link']]['b'] = +1

                metrics[events['timestamp']][record['link']]['d'] = metrics[events['timestamp']][
                    record['link']]['b']

                metrics[events['timestamp']][record['link']]['t'] = 1 + metrics[events['timestamp']][
                    record['link']]['s'] - metrics[events['timestamp']][record['link']]['b'] + metrics[events['timestamp']][
                    record['link']]['r'] - pow(metrics[events['timestamp']][record['link']]['s'], metrics[events['timestamp']][record['link']]['b'])

                record['trust'] = metrics[
                    events['timestamp']][record['link']]['t']

                # Analytics data
                #self.link_analytics.append([events['timestamp'], record['link'], metrics[events['timestamp']][record['link']]['s'], metrics[events['timestamp']][record['link']]['b'], metrics[events['timestamp']][record['link']]['r'],metrics[events['timestamp']][record['link']]['t']])

                # Get current min and max
                cost_min = min(cost_min, record['trust'])
                cost_max = max(cost_max, record['trust'])

        # Update data trust_min and trust_max
        self.data['trust_min'] = cost_min
        self.data['trust_max'] = cost_max

        # Analytics
        #analytics.create_data_file(self.link_analytics)

        return metrics
