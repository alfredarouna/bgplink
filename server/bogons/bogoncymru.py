"""
BGP Link Evaluator
Copyright (c) 2017 Alfred Arouna
"""

import urllib2
import traceback
import httplib
import os.path
import time
import ssl


class BogonSet(object):
    """
    Bogon class to create bogon set. This set is used to check 
    if a prefix is bogon or not. 

    """

    def __init__(self):
        self.opener = urllib2.build_opener()
        self.opener.addheaders = [
            ('User-agent', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)')]
        self.bogon = set()
        self.current_fname = 'bogon_' + time.strftime("%Y%m%d") + '.txt'
        self.url = 'https://www.team-cymru.org/Services/Bogons/fullbogons-ipv4.txt'
        self.default_fname = 'fullbogons-ipv4.txt'
        self.fname = ''
        self.dir = 'bogons'

    def get_bogon(self, save):
        """
        Get bogon list from team cymru. Save local copy if required.
        Input: flag to save downloaded file localy or not
        Return: bogon set

        """
        try:
            resource = urllib2.urlopen(self.url)
            if resource.getcode() == 200:
                if save:
                    fileObj = open(self.fname, "w")

                for line in resource:
                    if line.startswith('#'):
                        continue
                    else:
                        self.bogon.add(''.join(line.splitlines()))
                        if save:
                            fileObj.write(line)

                if save:
                    fileObj.close()

        except urllib2.HTTPError, e:
            print('HTTPError = ' + str(e.code))
        except urllib2.URLError, e:
            print('URLError = ' + str(e.reason))
        except httplib.HTTPException, e:
            print('HTTPException')
        except Exception:
            print('generic exception: ' + traceback.format_exc())

        return self.bogon

    def read_bogon(self, save):
        """
        Create bogon set by:
            - getting bogon prefixes from downloaded file (team cymru)
            - reading local bogon file (if not internet access).
        Input: flag to save downloaded file localy or not
        Return: bogon set
        Require: get_bogon function

        """
        if os.path.isfile(self.dir + '/' + self.current_fname) or os.path.isfile(self.dir + '/' + self.default_fname):
            if os.path.isfile(self.dir + '/' + self.current_fname):
                self.fname = self.current_fname
            if os.path.isfile(self.dir + '/' + self.default_fname):
                self.fname = self.default_fname
            with open(self.dir + '/' + self.fname) as f:
                for l in f:
                    self.bogon.add(l)

            return self.bogon
        else:
            self.get_bogon(True)
