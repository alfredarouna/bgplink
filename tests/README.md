Docker image for testing
========================

Prerequisite
------------
* Running docker engine: `sudo docker -v`
* Repository clone: `git clone https://alfredarouna@bitbucket.org/alfredarouna/bgplink.git`
* Switch to Alpha release: `cd bgplink && git checkout -b v0.7`

Build new image
---------------
To buid new docker image, you just need to run the following command:

`sudo docker build -f tests/Dockerfile -t link-evaluator-img .`

Run container and expose ports on host
--------------------------------------
Based on the image, you can create new container and expose this container ports to running host:

`sudo docker run -d -p 80:80 -p 8080:8080 --name link-evaluator link-evaluator-img`

Testing
-------

* Open your browser and enter your laptop/pc IP in location bar. You may see bgplink client interface.
* Select the type (incident or control) data from each test case.
* The specific data will be load and a modified version of BGPlayJs will display neutral links.
* A click on animation player icon, will start updating link color based on link reputation cost.

Screenshots
-----------
* Egypt country-wide outage

![First event at 21:00 UTC (January 27th, 2011)](egypt_disconnect_full_start.png)
![Last event at 23:00 UTC (January 27th, 2011)](egypt_disconnect_full_end.png)

* Pakistan hijack on Youtube prefixe

![First event at 19:00 UTC (February 24th, 2008)](pakistan_hijack_start.png)
![Last event at 20:50 UTC (February 24th, 2008)](pakistan_hijack_end.png)

* Link Telecom incident

![First event at 08:00 UTC (August 24th, 2011)](link_incident_start.png)
![Last event at 20:50 UTC (August 24th, 2011)](link_incident_end.png)

* Malaysia Telecom routes leak

![First event at 08:43 UTC (June 12th, 2015)](malaysia_leak_start.png)
![Last event at 10:45 UTC (June 12th, 2015)](malaysia_leak_end.png)

