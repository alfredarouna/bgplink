BGPLink (client)
================

As BGPlay.js, BGPLink.js provides a graphical and intuitive representation of the inter-domain routing and its evolution over time. When BGPlays.js display is based on AS_PATH, BGPLink.js is based on link between nodes. So for the same AS_PATH, BGPLink drawn each link indivually. 

Base on custom ***link reputation cost*** algorithm, each link get specifiq color:

    * yellow to red implies a link with bad reputation,
    * green to yellow represent a link with good reputation,
    * yellow is used for neutral link.

 
It has been developed as a part of the Master’s thesis of Alfred Arouna at Centre de Formation et de Recherche en Informatique (CeFRI) of University d'Abomey-Calavi (Benin).

The project is ***open source, see LICENSE.txt*** for more information.


