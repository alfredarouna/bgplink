/*
 * BGPlay.js
 * Copyright (c) 2013 Massimo Candela, Giuseppe Di Battista, Claudio Squarcella, Roma Tre University and RIPE NCC
 * http://www.bgplayjs.com
 *
 * See the file LICENSE.txt for copying permission.
 */

/**
 * This class is a wrapper of the inputs.
 * @class JsonWrap
 * @module model
 */
function JsonWrap(environment){
    return {

        getType: function(){
            return "bgp";
        },

        confirm: function(data){

            return (data.events.length + data.initial_state.length > environment.config.safetyMaximumEvents || data.nodes.length > environment.config.safetyMaximumNodes);
        },

        _getConversionList: function(){

            return conversionList = {
                starttimestamp: "starttime",
                endtimestamp: "endtime",
                targets: "resource",
                selectedRrcs: "rrcs"
            };
        },

        _fromExternalToInternal: function(external, internal){
            var out, conversionList, keyUsed;

            out = internal || {};
            keyUsed = {};
            conversionList = this._getConversionList();

            for (var key in conversionList){
                out[key] = external[conversionList[key]];
                keyUsed[conversionList[key]] = true;
            }

            for (var key in external){
                if (!keyUsed[key]){
                    out[key] = external[key];
                }
            }

            return out;
        },

        _fromInternalToExternal: function(internal, external){
            var out, conversionList;

            out = external || {};


            conversionList = this._getConversionList();

            for (var key in conversionList){
                out[conversionList[key]] = internal[key];
            }

            for (var key in internal){
                if (!out[conversionList[key]]){
                    out[key] = internal[key];
                }
            }

            return out;
        },

        /**
         * This method converts input parameters.
         * @param {Map} A Map of parameters valid outside the environment
         * @return {Map} A Map of parameters valid inside the environment
         */
        getParams:function(data){
            var out, params, internalParams, selectRRCset;

            out = {};

            params = this.externalParams || environment.thisWidget.get_params();
            internalParams = environment.params || {};

            selectRRCset = function(resources, currentValue){
                var value, thereIsAS;

                if (currentValue && currentValue.length > 1){
                    value = currentValue;
                } else {
                    if (typeof resources == "string"){
                        thereIsAS = !isNaN(resources.replace(/AS/g, ""));
                    } else {
                        for (var n=0,length=resources.length; n<length;n++){
                            thereIsAS = !isNaN(resources[n].replace(/AS/g, ""));
                            if (thereIsAS){
                                break;
                            }
                        }
                    }

                    value = (thereIsAS) ? environment.config.selectedRrcsAS : environment.config.selectedRrcsPrefix;
                }

                return (typeof value == "string") ? value : arrayToString(value);
            };

            if (!data){
                out = this._fromExternalToInternal(params, internalParams);
                out.selectedRrcs = selectRRCset(out.targets, params.rrcs);
            }else{

                if (params.unix_timestamps == "TRUE" || params.unix_timestamps == "true"){  //toUpperCase fails when unix_timestamps is null

                    out = {
                        starttimestamp: params.starttime || data.query_starttime,
                        endtimestamp: params.endtime || data.query_endtime,
                        showResourceController: params.showResourceController,
                        targets: ((typeof data.resource === 'string') ? data.resource : arrayToString(data.resource)),
                        //selectedRrcs: ((params.rrcs != null) ? params.rrcs : selectRRCset(data.resource, params.rrcs)),
                        selectedRrcs: ((params.rrcs != null) ? params.rrcs : createRRC(data.sources)),
                        ignoreReannouncements: (params.hasOwnProperty("ignoreReannouncements")) ? (params.ignoreReannouncements == "true") : environment.config.ignoreReannouncementsByDefault,
                        instant: params.instant,
                        preventNewQueries: params.preventNewQueries,
                        nodesPosition: params.nodesPosition,
                        datatype: params.datatype || null,
                        type: "bgp"
                    };

                }else{
                    alert('Unix timestamps needed!');
                }
            }

            return out;
        },

        /**
         * This method converts input parameters.
         * @param {Map} A Map of parameters valid inside the environment
         * @return {Map} A Map of parameters valid outside the environment
         */
        setParams:function(params){
            var out = environment.thisWidget.get_params();

            out.unix_timestamps="TRUE";

            if (params.starttimestamp != null) out.starttime = "" + params.starttimestamp;
            if (params.endtimestamp != null) out.endtime = "" + params.endtimestamp;
            if (params.targets != null) out.resource = "" + params.targets;
            if (params.showResourceController!=null) out.showResourceController = "" + params.showResourceController;
            if (params.selectedRrcs != null) out.rrcs = "" + params.selectedRrcs;
            if (params.ignoreReannouncements != null) out.ignoreReannouncements = "" + (params.ignoreReannouncements == true);
            out.instant = "" + params.instant;
            if (params.preventNewQueries != null) out.preventNewQueries = "" + params.preventNewQueries;
            if (params.nodesPosition != null) out.nodesPosition = "" + params.nodesPosition;
            if (params.datatype != null) out.datatype = "" + params.datatype;
            out.type="bgp";

            environment.thisWidget.set_params(out);
            return out;
        },

        /**
         * This method returns the URL where the json file related to the provided parameters is placed.
         * @param {String} A string where the data-source is placed
         * @param {Map} A Map of parameters valid inside the environment
         * @return {String} An URL
         */
        getJsonUrl:function(params){

            printLoadingInformation(environment,"Collecting data...");

            //var datasource = "https://stat.ripe.net/data/bgplay/data.json";
            /*return datasource + "?" +
                "unix_timestamps=TRUE" +
                "&type=bgp" +
                "&resource=" + params.targets +
                ((params.selectedRrcs != null)? "&rrcs=" + params.selectedRrcs : "") +
                ((params.endtimestamp != null)? "&endtime=" + params.endtimestamp : "") +
                ((params.starttimestamp != null)? "&starttime=" + params.starttimestamp : "");
            */
             
            //Use for testing. Just send json data
            //var datasource = "http://192.168.56.101:8080/datadirect.json";
            var datasource = params.datasource;
            return datasource + "?" +
                "unix_timestamps=TRUE" +
                "&type=bgp" +
                "&datatype="+ params.datatype +
                "&resource=" + params.targets;
            
            // Valid url. Get params
            /*var datasource = "http://192.168.56.101:8080/data.json";
            return datasource + "?" +
                "unix_timestamps=TRUE" +
                "&type=bgp" +
                "&resource=" + params.targets;*/

        },


        /**
         * This method populates Bgplay instantiating all the object of the model layer.
         * @param {Object} A json data object
         */
        readJson:function(wrap){
            if (wrap.nodes.length == 0){
                //console.log("no nodes");
                if (environment.thisWidget){
                    environment.message = {text: "No information available for these query parameters.", type:"info"};
                    return false;
                }else{
                    alert('No information available');
                    document.location=environment.config.homeUrl;
                }
            }

            printLoadingInformation(environment,"Reconstructing the history.");
            environment.bgplay = new Bgplay({
                cur_instant:new Instant({
                    id:0,
                    timestamp: wrap.query_starttime
                }),
                starttimestamp: wrap.query_starttime,
                endtimestamp: wrap.query_endtime,
                maxrank: wrap.trust_max,
                minrank: wrap.trust_min,
                type:"bgp"
            });
            window.bgplay = environment.bgplay;
            var bgplay = environment.bgplay;

            function createNodes(wrap){
                var node, asnumber, newnode, length, nodes, n;
                nodes = wrap.nodes;
                length = nodes.length;

                for (n=0; n<length; n++){
                    node = nodes[n];
                    asnumber = node.as_number;
                    newnode = new Node({id:asnumber, asnumber:asnumber, as:asnumber, owner:node.owner, nodeUrl:"https://stat.ripe.net/AS"+asnumber, environment:environment});
                    bgplay.addNode(newnode);
                }
            }

            function createSources(wrap){
                var source, sourceNode, newsource, length, sources, n;
                sources = wrap.sources;
                length = sources.length;
                for (n=0; n<length; n++){
                    source = sources[n];
                    sourceNode = bgplay.getNode(source.as_number);
                    newsource = new Source({id:source.id, group:sourceNode, environment:environment});
                    bgplay.addSource(newsource);
                    if (sourceNode!=null)
                        sourceNode.addSource(newsource);
                }
            }

            function createTargets(wrap){
                var target, targets, length, n, newtarget;
                targets = wrap.targets;
                length = targets.length;
                for (n=0; n<length; n++){
                    target = targets[n];
                    newtarget = new Target({id:target.prefix, environment:environment});
                    bgplay.addTarget(newtarget);
                }
            }


            function createInitialState(wrap){
                var path, event, initialstate, source, tmpPath, target, tmpNode, states, length, uniquePath, n, length2, i, uniqueTarget;

                uniquePath = [];
                states = wrap.initial_state;
                length = states.length;
                
                //target = bgplay.getTarget(uniqueTarget);
                //console.log(target);
                for (n=0; n<length; n++){
                    initialstate = states[n];

                    target = bgplay.getTarget(initialstate.target_prefix);
                    source = bgplay.getSource(initialstate.source_id);

                    if (initialstate.path.length == 0){
                        continue;
                    }

                    path = new Path({
                        id: n,
                        announcedPath: initialstate.path.join(" "),
                        target: target,
                        source: source,
                        environment: environment
                    });
                    uniquePath[source.id + "-" + target.id] = path;

                    tmpPath = initialstate.path;
                    //console.log(tmpPath);
                    //console.log(tmpPath.length);

                    tmpNode = bgplay.getNode(tmpPath[tmpPath.length - 1]);
                    tmpNode.addTarget(target); //In this way we can check hijacking
                    if (!arrayContains(target.get("nodes"), tmpNode)){
                        target.addNode(tmpNode);
                    }

                    length2 = tmpPath.length;
                    for (i= 0; i<length2; i++){
                        if (!tmpPath[i-1] || tmpPath[i-1] != tmpPath[i]){
                            path.addNode(bgplay.getNode(tmpPath[i]));
                        }
                    }
                    event = new Event({
                        subType: "initialstate",
                        type: "initialstate",
                        source: source,
                        target: target,
                        path: path,
                        instant: bgplay.get("cur_instant"),
                        
                        environment: environment
                    });
                    

                    //console.log(initialstate.path);
                    for(a=0;a<initialstate.path.length-1;a++){
                        tmpNode1 = bgplay.getNode(initialstate.path[a]);
                        tmpNode2 = bgplay.getNode(initialstate.path[a+1]);
                        //console.log(initialstate.path[a]+':'+initialstate.path[a+1]);
                        linkRank = new LinkRank({
                            link: initialstate.path[a]+':'+initialstate.path[a+1],
                            rank: null

                        });

                        linkRank.addNode(tmpNode1);
                        linkRank.addNode(tmpNode2);
                        
                        //console.log(initialstate.path[a]+':'+initialstate.path[a+1] == '3549:4788');

                        event.addRank(linkRank);

                        //if(initialstate.path[a]+':'+initialstate.path[a+1] == '12637:3257'){
                        //    console.log(source);
                        //    console.log(target);
                        //}
                        
                        //console.trace();
                    }

                    source.addEvent(event);
                    bgplay.get("allEvents").put(bgplay.get("cur_instant"), event);

                }

                return uniquePath;
            }

            function createEvents(uniquePath, wrap){
                var eventId = 1;
                var events, event, n, i, length2, tmpNode, instant, eventType, currentPath, attributes, shortdescription,
                    source, longdescription, path, target, tmpEvent, prevPath, tmpPath, subType, numNotValidWithdrawal, length;
                var ignoreReannouncements = environment.params.ignoreReannouncements;
                var pathStartId = uniquePath.length;
                var linkRank;


                numNotValidWithdrawal = 0;
                

                events = wrap.events;
                length = events.length;
                for (n=0; n<length; n++){
                    event = events[n];
                    eventType = event.type;
                    attributes = event.attrs;
                    source=bgplay.getSource(attributes.source_id);
                    target=bgplay.getTarget(attributes.target_prefix);
                    records = event.records;

                    /*for(a=0;a<records.length;a++){

                        linkRank = new linkRank({
                            id: records[a].link,
                            rank: records[a].rank
                        });

                    }*/
                    //console.log(ranks);

                    prevPath = uniquePath[source.id + "-" + target.id];

                    //console.log(source.id + "-" + target.id);
                    //console.log(prevPath);
                    //console.log(prevPath);
                    //console.log('');
                    

                    if (eventType=='W' && prevPath==null){
                        numNotValidWithdrawal++;
                        continue;
                    }

                    if (attributes.path!=null && attributes.path.length==0){
                        continue;
                    }

                    currentPath = (attributes.path) ? attributes.path.join(" ") : "";
                    instant = new Instant({
                        id:eventId,
                        timestamp: event.timestamp,
                        environment:environment
                    });
                    path = new Path({
                        id: pathStartId,
                        announcedPath: currentPath,
                        target: target,
                        source: source,
                        environment: environment
                    });//n is a good id (must be integer)
                    pathStartId++;
                    //console.trace();
                    //console.log(path);
                    tmpEvent = new Event({
                        source: source,
                        target: target,
                        type: event.type,
                        instant: instant,
                        community: (attributes.community) ? attributes.community.join(",") : null,
                        environment: environment
                    });

                    //console.log(tmpEvent);

                    for(a=0;a<records.length;a++){
                        nodes = records[a].link.split(':');
                        tmpNode1 = bgplay.getNode(nodes[0]);
                        tmpNode2 = bgplay.getNode(nodes[1]);



                        linkRank = new LinkRank({
                            link: records[a].link,
                            rank: records[a].trust,

                        });

                        linkRank.addNode(tmpNode1);
                        linkRank.addNode(tmpNode2);
                        
                        //if(records[a].link == '3549:4788' && source.id == 'route-views4-205.166.205.202' && target.id == '202.188.192.0/19'){
                        //    console.log(tmpEvent.cid);
                        //}
                        //if(records[a].link == '12637:3257'){
                        //    console.log(source);
                        //    console.log(target);
                        //    console.log(tmpEvent.cid);
                        //}

                        tmpEvent.addRank(linkRank);
                        
                        //console.trace();
                    }


                    tmpPath = attributes.path;

                    if (eventType=='W' && prevPath!=null){
                        shortdescription = "The route "+ prevPath.toString()+" has been withdrawn.";
                        longdescription = "The route "+ prevPath.toString()+" has been withdrawn...more";
                        subType = "withdrawal";
                        tmpEvent.attributes.path = null;
                    //}else if (eventType=='A' || eventType=='B'){
                    }else if (eventType=='A' || eventType=='R'){

                        tmpNode = bgplay.getNode(tmpPath[tmpPath.length-1]);
                        tmpNode.addTarget(target);//In this way we can check hijacking
                        if (!arrayContains(target.get("nodes"),tmpNode)){
                            target.addNode(tmpNode);
                        }

                        length2 = tmpPath.length;

                        for (i=0; i<length2; i++){
                            if (!tmpPath[i-1] || tmpPath[i-1]!=tmpPath[i])
                                path.addNode(bgplay.getNode(tmpPath[i]));
                        }

                        if (prevPath==null){
                            shortdescription = "The new route "+ path.get('announcedPath')+" has been announced";
                            longdescription = "The new route "+ path.get('announcedPath')+" has been announced..more";
                            subType = "announce";
                            tmpEvent.attributes.path = path; //The new path
                        }else{
                            if (prevPath.toString()==path.toString()){
                                if (prevPath.get('announcedPath')==path.get('announcedPath')){
                                    if (!ignoreReannouncements){
                                        shortdescription = "The route "+ prevPath.get('announcedPath')+" has been announced again";
                                        longdescription = "The route "+ prevPath.get('announcedPath')+" has been announced again..more";
                                        subType = "reannounce";
                                        tmpEvent.attributes.path = prevPath; //The previous path
                                    }else{
                                        continue; //skip re-announcements
                                    }
                                }else{
                                    shortdescription = "The route "+ prevPath.get('announcedPath')+" introduced/removed prepending "+path.get('announcedPath');
                                    longdescription = "The route "+ prevPath.get('announcedPath')+" introduced/removed prepending "+path.get('announcedPath')+" ..more";
                                    subType = "prepending";
                                    tmpEvent.attributes.path = path;
                                }
                            }else{
                                shortdescription = "The route "+ prevPath.get('announcedPath')+" is changed to "+path.get('announcedPath');
                                longdescription = "The route "+ prevPath.get('announcedPath')+" is changed to "+path.get('announcedPath')+" ..more";
                                subType = "pathchange";
                                tmpEvent.attributes.path = path;//The new path
                            }
                        }
                    }
                    uniquePath[source.id+"-"+target.id] = tmpEvent.attributes.path;
                    tmpEvent.attributes.shortdescription = shortdescription;
                    tmpEvent.attributes.longdescription = longdescription;
                    tmpEvent.attributes.prevPath = prevPath;
                    tmpEvent.attributes.subType = subType;
                    source.addEvent(tmpEvent);
                    bgplay.get("allEvents").put(instant,tmpEvent);
                    eventId++;
                }

                if (numNotValidWithdrawal>0){
                    if (numNotValidWithdrawal == 1){
                        environment.cssAlert.alert("A withdrawal applied to a not existent path","warning",3000);
                    }else{
                        environment.cssAlert.alert(numNotValidWithdrawal+"  withdrawals ignored: no referenced path","warning",3000);
                    }
                }

                return [uniquePath, pathStartId];
            }

            createNodes(wrap);
            createSources(wrap);
            createTargets(wrap);
            var expose = createEvents(createInitialState(wrap), wrap);

            environment.uniquePath = expose[0];
            environment.pathStartId = expose[1];



            bgplay.updateState();

            return true;
        }
    }
}
