/*
 * BGPlay.js
 * Copyright (c) 2017 Alfred Arouna, 2013 Massimo Candela, Giuseppe Di Battista, Claudio Squarcella, Université d'Abomey-Calavi, Roma Tre University and RIPE NCC
 * http://www.bgplayjs.com
 *
 * See the file LICENSE.txt for copying permission.
 */

/**
 * @class LinkRank
 * @module model
 */
var LinkRank = Backbone.Model.extend({

    /**
     * The initialization method of this object.
     * @method initialize
     * @param {Map} A map of parameters
     */
    initialize:function(){
        this.fastSearchNodes = {};
        this.attributes.nodes = [];
    },

    /**
     * Adds a node to this path.
     * @method addNode
     * @param {Object} An instance of Node
     */
    addNode:function(node){
        this.get("nodes").push(node);
    },

    getId:function(){
        return this.get("id");
    },

    getRank:function(){
        return this.get("rank");
    },

    getNodes:function(){
        return this.attributes.nodes;
    },

    getLink:function(){
        return this.get("link");
    },

    /**
     * Checks if this path contains a given node.
     * @method contains
     * @param {Object} An instance of Node
     * @return {Boolean} True if the path contains the given node.
     */
    contains:function(element){
        //check execution time here:
        // http://jsperf.com/indexof-vs-loop/2
        // http://jsperf.com/various-loop

        var nodes=this.attributes.nodes;
        var length=nodes.length;
        for (var i=length;i--;){
            if (nodes[i].id==element.id)
                return true;
        }
        return false;
    },

    /**
     * The validation method of this object.
     * This method is used to check the initialization parameters.
     * @method validate
     * @param {Map} A map of parameters
     * @return {Array} An array of {String} errors
     */
    validate:function(attrs){
        var err=[];

        // Add rank[link]=value 
        if(attrs.rank == null)
            err.push("Rank cannot be null!");    

        if (err.length>0)
            return err;
    }
});








